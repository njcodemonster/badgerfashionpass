from django.apps import AppConfig


class BadgerConfig(AppConfig):
    name = 'Badger'
