from django.db import models
from mongoengine import * 
# Create your models here.
from mongoengine import Document, EmbeddedDocument
from mongoengine import fields

import datetime


class Comment(EmbeddedDocument):
    created_at = fields.DateTimeField(
        default=datetime.datetime.now, editable=False,
    )
    author = fields.StringField(verbose_name="Name", max_length=255)
    email  = fields.EmailField(verbose_name="Email", blank=True)
    body = fields.StringField(verbose_name="Comment")

class SelectTest(Document):
    created_at = fields.DateTimeField(
        default=datetime.datetime.now, editable=True,
    )
    name = fields.StringField(verbose_name="Name_other", max_length=255)
    name_f = fields.StringField(verbose_name="Name", max_length=255)
    type_cf = fields.IntField(min_value=0)
    type_f = fields.StringField(verbose_name="type", max_length=255)
    comment = EmbeddedDocumentField(Comment)
class test_1(Document):
    
    created_at = fields.DateTimeField(
        default=datetime.datetime.now, editable=True,
    )
    comment = EmbeddedDocumentField(Comment)
    name = fields.StringField(verbose_name="Name_other", max_length=255)
    name_f = fields.StringField(verbose_name="Name", max_length=255)
    type_cf = fields.IntField(min_value=0)
    type_f = fields.StringField(verbose_name="type", max_length=255)
    
   