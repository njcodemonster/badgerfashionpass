from django.db import models
from mongoengine import Document, EmbeddedDocument
from mongoengine import fields
import datetime
# Create your models here.
#----------------product--------------------------
class Product_Barcode(EmbeddedDocument):
    barcode_value = fields.StringField(verbose_name="barcode_value", max_length=255)
    barcode_status = fields.StringField(verbose_name="barcode_status", max_length=255)
    barcode_id = fields.ObjectIdField(required=False)
    created_date=fields.DateTimeField()
class Product_Sku(EmbeddedDocument):
    sub_sku_id = fields.StringField(verbose_name="sub-skuid in this family of skus",max_length=255)
    barcodes = fields.EmbeddedDocumentListField(Product_Barcode)
class Product_Size(EmbeddedDocument):
    name = fields.StringField(max_length=255)
    displayname = fields.StringField(max_length=255)
    specificprice = fields.DecimalField()
    waight = fields.DecimalField()
    product_sku = fields.EmbeddedDocumentListField(Product_Sku)
class product_color(EmbeddedDocument):
    name=fields.StringField(max_length=255)
    code=fields.StringField(max_length=255)
class product_vendor(EmbeddedDocument):
    vendor_sku=fields.StringField(max_length=255)
    vendor_id=fields.ObjectIdField()
    vendor_name=fields.StringField(max_length=255)
class product_made(EmbeddedDocument):
    stuf=fields.ListField()
    discription=fields.StringField(max_length=255)
class product_typetags(EmbeddedDocument):
    tag_id=fields.StringField(max_length=255)
    tag=fields.StringField(max_length=255)
    displaytag=fields.StringField(max_length=255)
    taglink = fields.ObjectIdField()
class product_image(EmbeddedDocument):
    image_id=fields.StringField(max_length=255)
    title=fields.StringField(max_length=255)
    link=fields.StringField(max_length=255)
    active=fields.DecimalField()
    created_date=fields.DateTimeField()
    update_date=fields.DateTimeField()
    type=fields.StringField(max_length=255) #make this default to normal other value is promo
class product_comments(EmbeddedDocument):
    comment_id=fields.StringField(max_length=255)
    text=fields.StringField(max_length=2000)
    commnet_user_name=fields.StringField(max_length=255)
    comment_user_id=fields.ObjectIdField()
    created_date=fields.DateTimeField()
    update_date=fields.DateTimeField()
class Products(Document):
    _id = fields.StringField(verbose_name="product_id_pk", max_length=255)
    usedinorders= fields.ListField()
   # barcode_id = fields.EmbeddedDocumentListField(purchaseorder_ids)
    name = fields.StringField(verbose_name="product_name", max_length=255)
    title = fields.StringField(verbose_name="product_title", max_length=255)
    slug = fields.StringField(verbose_name="product_slug", max_length=255)
    parent_sku = fields.StringField(verbose_name="product_sku", max_length=255)
    cost = fields.DecimalField()
    price = fields.DecimalField()
    retail_price=fields.DecimalField()
    rental_price =fields.DecimalField()
    sizes = fields.EmbeddedDocumentListField(Product_Size)
    color= fields.EmbeddedDocumentField(product_color)
    made = fields.EmbeddedDocumentField(product_made)
    type = fields.StringField( max_length=255)
    sub_type =fields.StringField( max_length=255)
    vendor = fields.EmbeddedDocumentField(product_vendor)
    images = fields.EmbeddedDocumentField(product_image)
    typetags = fields.EmbeddedDocumentListField(product_typetags)
    pairwith = fields.ListField()
    color_pair = fields.ListField()
    complete_the_look = fields.ListField()
    comments_notes = fields.EmbeddedDocumentField(product_comments)
    last_event = fields.ObjectIdField()
    created_date=fields.DateTimeField()
    update_date=fields.DateTimeField()

#---------------------------------------vendor--------------------------------   
class vendor_address(EmbeddedDocument):
    street = fields.StringField(verbose_name="street", max_length=255)
    ubit = fields.StringField(verbose_name="ubit", max_length=255)
    city = fields.StringField(verbose_name="city", max_length=255)
    zip = fields.IntField(verbose_name="zip", max_length=255)
    state = fields.StringField(verbose_name="state", max_length=255)
class vendor_representatives(EmbeddedDocument):
    #_id = fields.StringField(verbose_name="street", max_length=255)
    name = fields.StringField(verbose_name="vendername", max_length=255)
    email = fields.StringField(verbose_name="city", max_length=255)
    phone = fields.StringField(verbose_name="phonenumber", max_length=255)
    primary = fields.IntField()
class vendor_orders(EmbeddedDocument):
    order_id = fields.StringField(verbose_name="order_id", max_length=255)
    created_at = fields.DateTimeField(verbose_name="order_created")
class Vendors(Document):
    #_id = fields.StringField(verbose_name="vendor_id_pk", max_length=255)
    name = fields.StringField(verbose_name="vendor_name", max_length=255)
    code = fields.StringField(verbose_name="vendor_code", max_length=255)
    slug = fields.StringField(verbose_name="vendor_slug", max_length=255)
    customernumber = fields.StringField(verbose_name="customernumber", max_length=255)
    status = fields.StringField(verbose_name="vendor_status", max_length=255)
    created_at = fields.DateTimeField(verbose_name="vendor_created")
    notes= fields.StringField(verbose_name="vendor_notes", max_length=255)#ubaid make it embaded doc as I have made for other notes
    address = fields.EmbeddedDocumentListField(Product_Size)#product size !!!!!!!???????????????????
    vendor_address = fields.EmbeddedDocumentListField(vendor_address)#why two adresses !!!!???
    vendor_representatives = fields.EmbeddedDocumentListField(vendor_representatives)
    vendor_orders = fields.EmbeddedDocumentListField(vendor_orders)#make it JUST a lit not embaded document list please. 


#------------------------purchase order-------------------------------------


class PO_sizes_barcodes(EmbeddedDocument):
    barcode_id = fields.StringField(verbose_name="barcode_id", max_length=255)
    barcode_value = fields.StringField(verbose_name="barcode_value", max_length=255)
    barcode_status = fields.StringField(verbose_name="barcode_status", max_length=255)
    note = fields.StringField(verbose_name="note", max_length=255)
    note_img = fields.StringField(verbose_name="note_img", max_length=255)

class PO_sizes_sku(EmbeddedDocument):
    sub_sku_value = fields.StringField(verbose_name="sub_sku_value", max_length=255)
    barcodes = fields.EmbeddedDocumentListField(PO_sizes_barcodes)

class PO_styles_sizes(EmbeddedDocument):
    name = fields.StringField(verbose_name="size_name", max_length=255)
    display_name = fields.StringField(verbose_name="size_displayname", max_length=255)
    vendor_size_name = fields.StringField(verbose_name="vendor_size_name", max_length=255)
    specific_price = fields.IntField(verbose_name="size_name", max_length=255)
    weight = fields.IntField(verbose_name="vendor_size_name", max_length=255)
    sizes = fields.EmbeddedDocumentListField(PO_sizes_sku)

class PO_styles(EmbeddedDocument):
    product_id = fields.StringField(verbose_name="product_id", max_length=255)
    style_name = fields.StringField(verbose_name="style_name", max_length=255)
    title = fields.StringField(verbose_name="style_title", max_length=255)
    color = fields.StringField(verbose_name="style_color", max_length=255)
    color_code = fields.StringField(verbose_name="style_color_code", max_length=255)
    vender_name = fields.StringField(verbose_name="vendor_name", max_length=255)
    vender_id = fields.StringField(verbose_name="vendor_id", max_length=255)
    unit_price = fields.IntField(verbose_name="style_unit_price", max_length=255)
    sugg_retail = fields.IntField(verbose_name="style_suggested_price", max_length=255)
    style_type = fields.StringField(verbose_name="style_type", max_length=255)
    sub_type = fields.StringField(verbose_name="style_sub_type", max_length=255)
    note = fields.StringField(verbose_name="style_note", max_length=255)
    display_iamge = fields.StringField(verbose_name="style_display_image", max_length=255)
    sizes = fields.EmbeddedDocumentListField(PO_styles_sizes)

class PO_documents(EmbeddedDocument):
    document_title = fields.StringField(verbose_name="PO_document_title", max_length=255)
    document_url = fields.StringField(verbose_name="PO_document_url", max_length=255)

class PO_stats(EmbeddedDocument):
    item_states_name =fields.StringField(verbose_name="PO_item_states", max_length=255)
    item_stats = fields.DecimalField()

class PO_delivery_range(EmbeddedDocument):
    start = fields.DateTimeField(verbose_name="from_range", max_length=255)
    end = fields.DateTimeField(verbose_name="to_range", max_length=255)

class Purchase_order(Document):
    vendor_name = fields.StringField(verbose_name="PO_vendor_name", max_length=255)
    vender_id = fields.StringField(verbose_name="PO_vender_id", max_length=255)
    PO_number = fields.StringField(verbose_name="PO_number", max_length=255)
    invoice_number = fields.StringField(verbose_name="PO_invoice_number", max_length=255)
    order_number = fields.StringField(verbose_name="PO_order_number", max_length=255)
    created_at = fields.DateTimeField(verbose_name="PO_created")
    payment_status = fields.StringField(verbose_name="PO_payment_status", max_length=255)
    tracking_number = fields.StringField(verbose_name="PO_tracking_number", max_length=255)
    notes= fields.StringField(verbose_name="PO_notes", max_length=255)
    delivery_range = fields.EmbeddedDocumentListField(PO_delivery_range)
    PO_stats = fields.EmbeddedDocumentListField(PO_stats)
    PO_documents = fields.EmbeddedDocumentListField(PO_documents)
    vendor_orders = fields.EmbeddedDocumentListField(PO_styles)




    
#------------------------barcode-------------------------------------
class barcode_notes(EmbeddedDocument):
    note_id=fields.StringField(max_length=255)
    text=fields.StringField(max_length=2000)
    note_user_name=fields.StringField(max_length=255)
    note_user_id=fields.ObjectIdField()
    created_date=fields.DateTimeField()
    update_date=fields.DateTimeField()
class barcode(Document):
    barcode_value = fields.StringField(max_length=255)
    notes = fields.EmbeddedDocumentField(barcode_notes)
    status = fields.StringField(max_length=255)
    PO_id = fields.ObjectIdField()
    product_id =fields.ObjectIdField()
    vendor_id=fields.ObjectIdField()
    sku =fields.StringField(max_length=255)
    created_date=fields.DateTimeField()
    update_date=fields.DateTimeField()
#-------------------------photoshoot-----------------------------------
class photoshoots(Document):
    model_name = fields.StringField(max_length=255)
    tottal_products_assigned=fields.DecimalField()
    created_date=fields.DateTimeField()
    update_date=fields.DateTimeField()
    assigned_products=fields.ListField()
