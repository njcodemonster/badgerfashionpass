from django.apps import AppConfig


class FashionpassConfig(AppConfig):
    name = 'fashionpass'
